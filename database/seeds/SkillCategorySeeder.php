<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\SkillCategory::class, 20)->create();

        DB::table('skill_categories')->insert(
            [
                'name_ru' => 'Без категории',
                'color' => ('0, 0, 0'),
            ]
        )
        ;
    }
}
