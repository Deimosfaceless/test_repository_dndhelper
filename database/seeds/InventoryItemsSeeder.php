<?php

use Illuminate\Database\Seeder;

class InventoryItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\InventoryItem::class, 20)->create();
    }
}
