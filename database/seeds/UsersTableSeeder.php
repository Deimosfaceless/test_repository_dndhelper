<?php

use App\Models\Hero;
use App\Models\InventoryItem;
use App\Models\Skill;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create()->each(
            function(User $user) {
                $heroes = factory(Hero::class, 3)->create()->each(
                    function(Hero $hero) {
                        $skills = factory(Skill::class, 7)->create();
                        $pivotAttributes = [];
                        foreach ($skills as $key => $value) {
                            $pivotAttributes[] = ['on_level' => $key + 1];
                        }
                        $hero->skills()->saveMany($skills, $pivotAttributes);

                        $availableSkills = factory(Skill::class, 7)->create();
                        $hero->availableSkills()->saveMany($availableSkills);

                        $inventoryItems = factory(InventoryItem::class, 7)->create();
                        $pivotAttributes = [];
                        foreach ($inventoryItems as $inventoryItem) {
                            $pivotAttributes[] = ['quantity' => rand(1, 20)];
                        }
                        $hero->inventoryItems()->saveMany($inventoryItems);
                    })
                ;
                $pivotAttributes = [];
                $userRoles = User::USER_ROLES;
                foreach ($heroes as $key => $hero) {
                    $pivotAttributes[] = ['relation' => $userRoles[mt_rand(0, count($userRoles) - 1)]];
                }
                $user->playableHeroes()->saveMany($heroes, $pivotAttributes);
            })
        ;

        //admin
        DB::table('users')->insert(
            [
                'email' => 'admin@lol.ru',
                'name' => 'admin',
                'role' => User::ADMIN_ROLE,
                'password' => Hash::make('123123'),
            ]
        )
        ;

        //first user
        DB::table('users')->insert(
            [
                'email' => 'user@lol.ru',
                'name' => 'user',
                'role' => User::PLAYER_ROLE,
                'password' => Hash::make('123123'),
            ]
        )
        ;
    }
}
