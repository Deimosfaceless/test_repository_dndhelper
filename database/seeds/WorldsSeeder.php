<?php

use App\Models\World;
use App\Models\WorldCategory;
use App\Models\WorldPage;
use Illuminate\Database\Seeder;

class WorldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(World::class, 5)->create();
        factory(WorldCategory::class, 20)->create();
        factory(WorldPage::class, 30)->create();
    }
}
