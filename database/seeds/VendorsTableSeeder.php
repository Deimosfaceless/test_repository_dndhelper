<?php

use App\Models\InventoryItem;
use App\Models\Vendor;
use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Vendor::class, 4)->create()->each(
            function(Vendor $vendor) {
                $inventoryItems = factory(InventoryItem::class, 20)->create();
                $pivotAttributes = [];
                foreach ($inventoryItems as $inventoryItem) {
                    $pivotAttributes[] = ['quantity' => rand(1, 20)];
                }
                $vendor->inventoryItems()->saveMany($inventoryItems, $pivotAttributes);
            })
        ;
    }
}
