<?php

use App\Models\WorldCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\WorldPage::class, function(Faker $faker) {
    $worldCategoryIds = WorldCategory::pluck('id')->toArray();

    return [
        'name' => $faker->word . '_worldPage',
        'description' => $faker->text,
        'content' => $faker->text,
        'world_category_id' => array_rand($worldCategoryIds),
    ];
});
