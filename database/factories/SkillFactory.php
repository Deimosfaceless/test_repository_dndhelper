<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\Skill::class, function(Faker $faker) {
    $skillCategoriesIds = \App\Models\SkillCategory::pluck('id')->toArray();

    return [
        'name_ru' => $faker->word . '_skill',
        'category_id' => array_rand($skillCategoriesIds),
    ];
});
