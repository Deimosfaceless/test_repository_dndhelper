<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\InventoryItem::class, function(Faker $faker) {
    return [
        'name' => $faker->word . '_item',
        'short_description' => $faker->text,
        'full_description' => $faker->text,
        'rare' => rand(0, 3),
    ];
});
