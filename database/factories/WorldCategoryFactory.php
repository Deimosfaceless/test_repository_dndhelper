<?php

use App\Models\World;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\WorldCategory::class, function(Faker $faker) {
    $worldIds = World::pluck('id')->toArray();

    return [
        'name' => $faker->word . '_worldCategory',
        'world_id' => array_rand($worldIds),
    ];
});
