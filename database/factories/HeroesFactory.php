<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\Hero::class, function(Faker $faker) {
    return [
        'name_ru' => $faker->word . '_name',
        'name_en' => $faker->word,
        'gold' => $faker->randomFloat(2, 0, 50),
    ];
});
