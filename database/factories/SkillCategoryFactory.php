<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\SkillCategory::class, function(Faker $faker) {
    return [
        'name_ru' => $faker->word . '_skillCategory',
        'color' => $faker->rgbColor,
        'author_id' => rand(0, 10),
    ];
});
