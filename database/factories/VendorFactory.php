<?php

use App\Models\Vendor;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    Vendor::class, function(Faker $faker) {
    return [
        'name_ru' => $faker->name,
        'markup' => rand(0, 10),
        'markup_percent' => rand(10, 50),
    ];
});
