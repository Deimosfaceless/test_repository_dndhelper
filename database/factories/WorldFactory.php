<?php

use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/** @var Factory $factory */
$factory->define(
    App\Models\World::class, function(Faker $faker) {
    $masterIds = User::master()->pluck('id')->toArray();

    return [
        'name' => $faker->word . '_world',
        'master_id' => array_rand($masterIds),
    ];
});
