<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('rare')->default(0); //Значение редкость, чтобы потом рандомить награду можно было
            $table->text('short_description')->nullable();
            $table->text('full_description')->nullable();
            $table->integer('author_id')->nullable();
            $table->integer('price')->nullable();
            $table->string('units')->nullable(); //в чём измеряется
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_items');
    }
}
