<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroAvailableSkillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hero_available_skill', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('hero_id');
            $table->integer('skill_id');
            $table->integer('available_on')->nullable(); //На каком уровне доступен навык
            $table->string('required_skills')->nullable(); //Требуемые навыки
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hero_available_skill');
    }
}
