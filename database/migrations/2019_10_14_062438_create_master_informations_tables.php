<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterInformationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worlds', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('master_id');
            $table->boolean('is_public')->default(false); //могут просматривать информацию все пользователи
            $table->timestamps();
        });

        Schema::table('heroes', function(Blueprint $table) {
            $table->integer('world_id')->nullable();
        });

        Schema::create('world_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('world_id');
            $table->integer('parent_id')->nullable();
            $table->timestamps();
        });

        Schema::create('world_pages', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('world_category_id');
            $table->string('description')->nullable();
            $table->text('content')->nullable();
            $table->integer('file_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worlds');
        Schema::dropIfExists('world_categories');
        Schema::dropIfExists('world_pages');

        Schema::table('heroes', function(Blueprint $table) {
            $table->dropColumn('world_id');
        });
    }
}
