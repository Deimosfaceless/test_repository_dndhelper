<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('creator_id')->comment('user id who create room');
            $table->string('creator_type');
            $table->string('title');
            $table->string('short_description')->nullable();
            $table->string('full_description')->nullable();
            $table->string('background_image')->nullable();
            $table->string('password')->nullable();
            $table->boolean('is_public')->comment('only for registered users')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
