<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_ru');
            $table->string('name_en')->nullable();
            $table->integer('parent')->nullable();
            $table->string('category_id')->nullable();
            $table->integer('one_of')->nullable(); // если дан навык на выбор то сюда заносить айди группы
            $table->integer('level')->nullable();
            $table->integer('for_hero')->nullable(); //уникальный навык, для какого героя появился первым.
            $table->text('description_ru')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_admin')->nullable();
            /* Кто создал скилл, чтобы потом при добавлении возможных скилов для героев, мастер мог видеть скиллы
            которые добавили другие мастера
             */
            $table->integer('author_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skills');
    }
}
