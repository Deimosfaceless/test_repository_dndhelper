<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/rooms', 'RoomController@allRooms');
Route::get('/rooms/create', 'RoomController@create')->name('rooms.create');
Route::post('/rooms/create', 'RoomController@store')->name('rooms.store');
Route::get('/rooms/join/{id}', 'RoomController@join')->name('rooms.join');
Route::get('/rooms/{id}', 'RoomController@room')->name('rooms.room');

Route::get('messages', 'RoomController@fetchMessages');
Route::post('messages', 'RoomController@sendMessage');

//TODO delete this (or move to admin middleware)
Route::post('/mc-setting', 'MainController@saveMcServer');
Route::get('/mc', 'MainController@mcIndex');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function(){
    Route::get('/', 'DashboardController@index')->name('index');

    Route::get('/setting', 'SettingController@index')->name('setting.index');
    Route::post('/setting/store', 'SettingController@store')->name('setting.store');

    Route::post('/setting/setwebhook', 'SettingController@setWebhook')->name('setting.setwebhook');
    Route::post('/setting/getwebhookinfo', 'SettingController@getWebhookInfo')->name('setting.getwebhookinfo');
});

Route::get('/', 'MainController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('heroes', 'Hero\MasterHeroController@heroes');

Route::get('/my-heroes', 'HeroController@index')->middleware('player')->name('heroes');
Route::get('/my-heroes/{id}', 'HeroController@show');

Route::get('/skills', 'SkillsController@index')->middleware('master')->name('skills');
Route::post('/skills/create', 'SkillsController@create')->middleware('master');

Route::get('/test', 'TestController@index')->name('test');


Route::post(Telegram::getAccessToken(), function(){
    Telegram::commandsHandler(true);
});

Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');
