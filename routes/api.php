<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function(){
    Route::group(['prefix' => 'auth'], function() {
        Route::post('login', 'Api\V1\AuthController@login');
        Route::post('login-social', 'Api\V1\AuthController@loginBySocial');
        Route::post('signup', 'Api\V1\AuthController@signup');
        Route::post('signup-social', 'Api\V1\AuthController@signup');
    });

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::group(['prefix' => 'master'], function(){
            Route::get('heroes', 'Api\V1\MasterController@getHeroesList');
        });
    });
});


Route::get('/v1/test', 'Api\V1\TestController@test');
