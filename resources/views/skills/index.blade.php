@extends('master')

@push('title')
    {{ __('Heroes') }}
@endpush

@push('scripts')
    <script src="{{ asset('js/hero.js') }}" defer></script>
@endpush

@section('header')
    {{--header--}}
    <nav class="main-header navbar-expand-lg navbar navbar-dark bg-dark">
        <ul class="nav bg-secondary justify-content-end">
            <li class="nav-item">
                <a class="btn btn-secondary active" href="#">{{ __('Skills') }}</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-secondary" href="#">Мои герои</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-secondary" href="#">Заметки</a>
            </li>
        </ul>
    </nav>
    {{--    end header--}}
@endsection

@section('content')
    <div class="main-container bg-dark container-fluid">
        <main>
            <div class="row">
                <div class="col-md-2 main-border-right main-left">
                    <div class="row">
                        <div class="col" align="center">
                        <span class="custom-text">
                    {{ __('Players and heroes') }}
                        </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <span class="text-light player-name">PlayerName</span>
                            <hr class="new-hr">
                            <span>Hero1</span> <br>
                            <span>Hero1</span>
                        </div>
                        <div class="w-100"></div>
                        <div class="col">
                            <span class="text-light player-name">PlayerName</span>
                            <hr class="new-hr">
                            <span>Hero1</span> <br>
                            <span>Hero1</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" align="center">
                            <button type="button" data-toggle="modal" data-target="#create-hero" class="btn btn-outline-secondary">
                                {{ __('Create Hero') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer id="footer" class="footer navbar-fixed-bottom">
            Footer
        </footer>
    </div>

{{--    Start Modal--}}
    <div class="modal fade" id="create-hero" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content bg-dark">
                <div class="modal-header">
{{--                    TODO знак вопросика справа, в котором написано: про высылаемый инвайт, если выбран айди. В другом случае - это просто герой без игрока--}}
                    <h5 class="modal-title text-center" id="exampleModalLongTitle"><span>{{ __('Create Hero') }}</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-hero-create" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-8">
                                <label for="hero_name"><span>Hero Name</span></label>
                                <input id="hero_name" class="btn-outline-light form-control{{ $errors->has('hero_name') ? ' is-invalid' : '' }}"
                                       name="hero_name" value="{{ old('hero_name') }}" required autofocus>
                                @if ($errors->has('hero_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('hero_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                    <label for="user_id"><span>User Id</span></label>
                                    <i title="Потом можно будет с этим что-то сделать" data-toggle="tooltip" class="far fa-question-circle question-icon"></i>
                                <input id="user_id" class="btn-outline-light form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}"
                                       name="user_id" value="{{ old('user_id') }}" required>
                                @if ($errors->has('user_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="hero_description"><span>Hero description</span></label>
                                <textarea class="form-control hero-description" name="hero_description" id="hero_description" cols="55"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
{{--    End Modal--}}


@endsection