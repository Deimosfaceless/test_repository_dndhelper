@extends('admin.layouts.master')

@section('title')
    Admin / Dashboard
@endsection

@section('content')
    <div class="col s10">
        <h1>
            <span>AdminPanel</span>
        </h1>
    </div>
@endsection

@section('body-scripts')
    <script>
        $( document ).ready(function() {
            $('.collapsible').collapsible();
            $(".dropdown-trigger").dropdown();
        });
    </script>
@endsection
