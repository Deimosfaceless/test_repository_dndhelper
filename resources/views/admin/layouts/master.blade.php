<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/materialize.js') }}"></script>
    <link href="{{ asset('css/materialize.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class="grey darken-2">
<section> <!-- Top menu -->
    <nav class="grey darken-3">
        <div class="container">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">DndHelper</a>
                <ul class="right hide-on-med-and-down">
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">{{ \Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                </ul>
                <!-- Dropdown Content -->
                <ul id="dropdown1" class="dropdown-content grey darken-3">
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
</section>


<div class="row">
    <div class="col s2">
        <section> <!-- Left Menu -->
            <div class="nav-left">
                <ul class="collapsible">
                    <li class="current">
                        <a href="{{ route('admin.index') }}">
                            <div class="collapsible-header"><i class="material-icons">assignment</i>Dashboard</div>
                        </a>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">public</i>Информация о мирах</div>
                        <div class="collapsible-body">
                            <ul>
                                <li class="element"><a href="">Миры</a></li>
                                <li class="element">Категории миров</li>
                                <li class="element">Страницы</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">speaker_notes</i>Записи</div>
                        <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
    @yield('content')
</div>
@yield('body-scripts')
</body>
</html>
