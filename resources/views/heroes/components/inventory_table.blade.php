<div class="row">
    <div class="col aling-self-center" align="center">
        <span class="custom-text">Inventory</span>
        <div class="table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table-dark table-striped table-bordered table-hover table-inventory">
                <thead>
                <tr>
                    <th style="width: 12%">#</th>
                    <th style="width: 64%">Name</th>
                    <th style="width: 12%">Quantity</th>
                    <th style="width: 12%">Handle</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>1</th>
                    <th>Azaza </th>
                    <th>123</th>
                    <th>X</th>
                </tr>
                @foreach($hero->inventoryItems as $item)
                    <tr>
                        <th>{{$loop->iteration + 1}}</th>
                        <th><span title="{{$item->full_description}}" data-toggle="tooltip">{{$item->name}}</span></th>
                        <th>{{$item->pivot->quantity}}</th>
                        <th>X</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

</div>