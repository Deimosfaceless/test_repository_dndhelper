<div class="row">
    <div class="col aling-self-center" align="center">
        <span class="custom-text">Skills</span>
        <div class="table-wrapper-scroll-y my-custom-scrollbar">
            <table class="table-dark table-inventory">
                <thead>
                </thead>
                <tbody>
                @foreach($skills as $skill)
                    <tr>
                        @for($i = 0; $i < count($skill_cats); $i++)
                            @if($i == $skill_cats[$skill->category_id])
                                <th>
                                    <div title="{{$skill->description}}" data-toggle="tooltip" style="background-color: rgb({{$skill->category->color ?? '0, 0, 0'}});" class="skill-item">
                                        <span class="custom-text-skill">
                                            {{$skill->name}}
                                        </span>
                                    </div>
                                </th>
                            @else
                                <th></th>
                            @endif
                        @endfor
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

</div>