@extends('master')

@push('title')
    {{ __('Heroes') }}
@endpush

@push('scripts')
    <script src="{{ asset('js/hero.js') }}" defer></script>
@endpush

@section('content')
    {{--header--}}
    <nav class="main-header navbar-expand-lg navbar navbar-dark bg-dark">
        <ul class="nav bg-secondary justify-content-end">
            <li class="nav-item">
                <a class="btn btn-secondary active" href="#">{{ __('Heroes') }}</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-secondary" href="#">Мои герои</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-secondary" href="#">Заметки</a>
            </li>
        </ul>
    </nav>
    {{--    end header--}}
    <div class="main-container bg-dark container-fluid">
        <div class="row">
            <div class="col-md-2 main-border-right main-left">
                {{ __('Masters') }}
            </div>
        </div>
        <footer id="footer" class="footer navbar-fixed-bottom">
            Footer
        </footer>
    </div>

@endsection