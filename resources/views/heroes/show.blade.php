@extends('master')

@push('title')
    {{$hero->name}}
@endpush

@push('scripts')
    <script src="{{ asset('js/hero.js') }}" defer></script>
@endpush

@section('content')
{{--header--}}
    <nav class="main-header navbar-expand-lg navbar navbar-dark bg-dark">
        <ul class="nav bg-secondary justify-content-end">
            <li class="nav-item">
                <a class="btn btn-secondary active" href="#">{{$hero->name}}</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-secondary" href="#">Мои герои</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-secondary" href="#">Заметки</a>
            </li>
        </ul>
    </nav>
{{--    end header--}}
    <div class="main-container bg-dark container-fluid">
        <div class="row">
            <div class="col-md-2 main-border-right main-left">
                <div class="row align-items-center">
                    <div class="hero-name col align-self-center" align="center">
                        <span class="custom-text">{{$hero->name}}</span>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col align-self-center">
                        <img class="img-fluid" src="/img/secret.png" alt="">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="hero-inventory col">
                        <div class="custom-text">Money: {{$hero->gold}} <img class="img-money" src="/img/gold.ico" alt=""></div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 main-center main-border-right">
                <div class="row">
                    <div class="col-auto"><button data-target="#inventory-table" type="button" class="btn active btn-secondary btn-hero-tab">Inventory</button></div>
                    <div class="col-auto"><button data-target="#skills-table" type="button" class="btn btn-secondary btn-hero-tab">Skills</button></div>
                </div>
                <div id="inventory-table" class="custom-tab ">
                    @component('heroes.components.inventory_table', ['hero' => $hero])
                    @endcomponent
                </div>
                <div id="skills-table" class="custom-tab custom-tab-hidden">
                    @component('heroes.components.skill_table', ['skills' => $skills, 'skill_cats' => $skill_cats])
                    @endcomponent
                </div>
            </div>
            <div class="col-md-1">
                zaazazaz
            </div>
{{--            <div class="col-md-7 main-fixed-height main-center">--}}
{{--                <div class="row">--}}
{{--                    <div class="hero-name col align-self-center" align="center">--}}
{{--                        <span class="custom-text">Skills</span>--}}
{{--                        @component('heroes.components.inventory_table', ['hero' => $hero])--}}
{{--                        @endcomponent--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-2 main-fixed-height main-right">--}}
{{--                <div class="row">--}}
{{--                    <div class="hero-name col align-self-center" align="center">--}}
{{--                        <span class="custom-text">Schools</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>

        <footer id="footer" class="footer navbar-fixed-bottom">
            Footer
        </footer>
    </div>

@endsection