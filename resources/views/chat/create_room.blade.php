@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <form action="{{ route('rooms.store') }}" method="POST">
                @csrf
                <input type="text" name="title" value="{{ old('title') }}">
                <input type="text" name="short_description" value="{{ old('short_description') }}">
                <button type="submit">Save</button>
            </form>
        </div>
    </div>
@endsection
