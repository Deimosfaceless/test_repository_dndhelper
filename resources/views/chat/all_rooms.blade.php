@extends('layouts.app')

@section('content')

    <div class="container">
        @foreach($rooms as $room)
        <div class="row">
            <span>{{$room->title}}</span> <a href="{{ route('rooms.join', $room->id) }}">Join</a>
        </div>
        @endforeach
    </div>
@endsection
