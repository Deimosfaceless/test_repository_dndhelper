<?php

namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class UserData
 *
 * @package App\Dto
 *
 * @property string $name
 * @property string $email
 * @property string|null $password
 * @property string|null $role
 */
class UserData extends DataTransferObject
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string|null
     */
    public $password;

    /**
     * @var string|null
     */
    public $role;
}
