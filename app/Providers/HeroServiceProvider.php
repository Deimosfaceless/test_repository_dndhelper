<?php

namespace App\Providers;

use App\Contracts\HeroContract;
use App\Service\HeroService;
use Illuminate\Support\ServiceProvider;

class HeroServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HeroContract::class, function($app){
            return new HeroService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return array
     */
    public function boot()
    {
        return [HeroContract::class];
    }
}
