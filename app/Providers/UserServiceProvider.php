<?php

namespace App\Providers;

use App\Contracts\UserManagerContract;
use App\Service\UserManager;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserManagerContract::class, function($app){
            return new UserManager();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return array
     */
    public function boot()
    {
        return [UserManagerContract::class];
    }
}
