<?php

namespace App\Broadcasting;

use App\Models\Room;
use App\User;

class RoomChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\User  $user
     * @return array|bool
     */
    public function join(User $user, Room $room)
    {
        return $room->users()->contains($user);
    }
}
