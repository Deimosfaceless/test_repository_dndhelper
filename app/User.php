<?php

namespace App;

use App\Models\Hero;
use App\Models\Message;
use App\Models\Room;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Laravel\Passport\HasApiTokens;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $role
 * @property string $email
 * @property string $password
 * @property Carbon|null $email_verified_at
 * @property string|null $link
 * @property string|null $nickname
 * @property int|null $telegram_id
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read BelongsToMany|Collection|Hero[] $playableHeroes
 * @property-read BelongsToMany|Collection|Hero[] $masteredHeroes
 * @property-read HasMany|Collection|Room[] $createdRooms
 *
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLink($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User whereNickname($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereTelegramId($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User master()
 *
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * Все доступные роли
     */
    public const ROLES = [
        self::ADMIN_ROLE, self::MASTER_ROLE, self::PLAYER_ROLE
    ];

    /**
     * Роли для игроков
     */
    public const USER_ROLES = [
        self::MASTER_ROLE, self::PLAYER_ROLE,
    ];

    public const PLAYER_ROLE = 'player';
    public const MASTER_ROLE = 'master';
    public const ADMIN_ROLE = 'admin';

    //TODO если дело дойдёт до монетизации - нужно будет вынести в БД, чтобы диначично менять можно было
    public const MAX_WORLDS = 3;
    public const MAX_WORLD_CATEGORIES = 5;
    public const MAX_WORLD_CATEGORY_PAGES = 20;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'link', 'nickname', 'telegram_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Герои непосредственно человека, игрока.
     *
     * @return BelongsToMany|Collection|Hero[]
     */
    public function playableHeroes()
    {
        return $this->belongsToMany(Hero::class, 'user_hero')->wherePivot('relation', '=', self::PLAYER_ROLE);
    }

    /**
     * Герои для которых человек - мастер
     *
     * @return BelongsToMany|Collection|Hero[]
     */
    public function masteredHeroes()
    {
        return $this->belongsToMany(Hero::class, 'user_hero')->wherePivot('relation', '=', self::MASTER_ROLE);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeMaster(Builder $query)
    {
        return $query->where('role', '=', self::MASTER_ROLE);
    }

    public function createdRooms()
    {
        return $this->hasMany(Room::class, 'creator_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }
}
