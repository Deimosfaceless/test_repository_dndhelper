<?php

namespace App\TelegramCommands;

use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'help';

    /**
     * @var string Command Description
     */
    protected $description = 'Help command, Get a list of commands';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $text = 'Привет. Здарова. Команды пока в разработке, давай позже попробуй.';

        $this->replyWithMessage(compact('text'));
    }
}
