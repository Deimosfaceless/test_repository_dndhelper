<?php

namespace App\TelegramCommands;

use Telegram\Bot\Commands\Command;

class DiceCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'dice';

    /**
     * @var string Command Description
     */
    protected $description = 'Dice';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        //$update = $this->getUpdate();
        //$userId = $update->items['message']['from']['id'];
        $chatId = -376383540;
        $evaId = 102043336;

        $text = (string) rand(0, 15);

        $this->replyWithMessage(compact('text'));
    }
}
