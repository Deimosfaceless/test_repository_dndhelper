<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\SkillCategory
 *
 * @property int $id
 * @property string $name_ru
 * @property string $color
 * @property int|null $parent_id
 * @property string|null $name_en
 * @property string|null $author_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|SkillCategory newModelQuery()
 * @method static Builder|SkillCategory newQuery()
 * @method static Builder|SkillCategory query()
 * @method static Builder|SkillCategory whereAuthorId($value)
 * @method static Builder|SkillCategory whereColor($value)
 * @method static Builder|SkillCategory whereCreatedAt($value)
 * @method static Builder|SkillCategory whereId($value)
 * @method static Builder|SkillCategory whereNameEn($value)
 * @method static Builder|SkillCategory whereNameRu($value)
 * @method static Builder|SkillCategory whereParentId($value)
 * @method static Builder|SkillCategory whereUpdatedAt($value)
 * @mixin Eloquent
 */
class SkillCategory extends Model
{
}
