<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InventoryItem
 *
 * @property int $id
 * @property string $name
 * @property int $rare
 * @property string|null $short_description
 * @property string|null $full_description
 * @property int|null $author_id
 * @property int|null $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereFullDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereRare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $units
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InventoryItem whereUnits($value)
 */
class InventoryItem extends Model
{
    //
}
