<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Мир, созданный мастером
 *
 * App\Models\World
 *
 * @property int $id
 * @property string $name
 * @property int $master_id
 * @property bool $is_public
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read Collection|Hero[] $heroes
 * @property-read int|null $heroes_count
 *
 * @method static Builder|World newModelQuery()
 * @method static Builder|World newQuery()
 * @method static Builder|World query()
 * @method static Builder|World whereCreatedAt($value)
 * @method static Builder|World whereId($value)
 * @method static Builder|World whereIsPublic($value)
 * @method static Builder|World whereMasterId($value)
 * @method static Builder|World whereName($value)
 * @method static Builder|World whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class World extends Model
{
    /**
     * @var string
     */
    protected $table = 'worlds';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'master_id', 'is_public', 'created_at', 'updated_at'
    ];

    /**
     * @return HasMany|Collection|Hero[]
     */
    public function heroes()
    {
        return $this->hasMany(Hero::class);
    }

    /**
     * @return HasMany|Collection|WorldCategory[]
     */
    public function categories()
    {
        return $this->hasMany(WorldCategory::class);
    }
}
