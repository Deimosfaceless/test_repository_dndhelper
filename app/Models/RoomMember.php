<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomMember extends Model
{
    public const MEMBER_TYPE_GUEST = 'guest';
    /**
     * @var array
     */
    protected $fillable = [
        'room_id', 'member_id', 'member_type'
    ];

    protected $table = 'room_member';

    public static function getGuestCurrentRoom()
    {
        return RoomMember::where('member_type', self::MEMBER_TYPE_GUEST)->where('member_id', session()->getId())->first();
    }

//    public function users()
//    {
//        return $this->morphedByMany(User::class, 'memberable');
//    }
}
