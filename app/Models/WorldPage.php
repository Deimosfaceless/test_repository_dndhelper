<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Информационные страницы о мире
 *
 * App\Models\WorldPage
 *
 * @property int $id
 * @property string $name
 * @property int $world_category_id
 * @property string|null $description
 * @property string|null $content
 * @property int|null $file_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|WorldPage newModelQuery()
 * @method static Builder|WorldPage newQuery()
 * @method static Builder|WorldPage query()
 * @method static Builder|WorldPage whereContent($value)
 * @method static Builder|WorldPage whereCreatedAt($value)
 * @method static Builder|WorldPage whereDescription($value)
 * @method static Builder|WorldPage whereFileId($value)
 * @method static Builder|WorldPage whereId($value)
 * @method static Builder|WorldPage whereName($value)
 * @method static Builder|WorldPage whereUpdatedAt($value)
 * @method static Builder|WorldPage whereWorldCategoryId($value)
 *
 * @mixin Eloquent
 */
class WorldPage extends Model
{
}
