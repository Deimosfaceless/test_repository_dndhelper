<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'message', 'sender_id', 'room_id',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }
}
