<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Информационные категории о мире
 *
 * App\Models\WorldCategory
 *
 * @property int $id
 * @property string $name
 * @property int $world_id
 * @property int|null $parent_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|WorldCategory newModelQuery()
 * @method static Builder|WorldCategory newQuery()
 * @method static Builder|WorldCategory query()
 * @method static Builder|WorldCategory whereCreatedAt($value)
 * @method static Builder|WorldCategory whereId($value)
 * @method static Builder|WorldCategory whereName($value)
 * @method static Builder|WorldCategory whereParentId($value)
 * @method static Builder|WorldCategory whereUpdatedAt($value)
 * @method static Builder|WorldCategory whereWorldId($value)
 *
 * @mixin Eloquent
 */
class WorldCategory extends Model
{
    /**
     * @return BelongsTo|World
     */
    public function world()
    {
        return $this->belongsTo(WorldCategory::class);
    }

    /**
     * @return HasMany|WorldPage
     */
    public function pages()
    {
        return $this->hasMany(WorldPage::class);
    }
}
