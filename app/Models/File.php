<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title', 'path', 'auth_by', 'size',
    ];

    /**
     * @var array
     */
    public $appends = ['url', 'uploaded_time', 'size_in_kb'];

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return Storage::disk('s3')->url($this->path);
    }

    public static function boot()
    {
        parent::boot();
        static::creating(
            function($file) {
                $file->auth_by = auth()->user()->id;
            });
    }
}
