<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Room extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'creator_id', 'creator_type', 'title', 'short_description', 'full_description', 'background_image', 'is_public'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'room_member', 'room_id', 'member_id')->where('room_member.member_type', '=', User::class);
    }
}
