<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * Любой торговец, магазин/лавка или что-то подобное
 * 
 * Class Vendor
 *
 * @package App\Models
 * @property int $id
 * @property float $markup
 * @property int $markup_percent
 * @property string $name_ru
 * @property string|null $name_en
 * @property string|null $author_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|InventoryItem[] $inventoryItems
 * @property-read int|null $inventory_items_count
 * @method static Builder|Vendor newModelQuery()
 * @method static Builder|Vendor newQuery()
 * @method static Builder|Vendor query()
 * @method static Builder|Vendor whereAuthorId($value)
 * @method static Builder|Vendor whereCreatedAt($value)
 * @method static Builder|Vendor whereId($value)
 * @method static Builder|Vendor whereMarkup($value)
 * @method static Builder|Vendor whereMarkupPercent($value)
 * @method static Builder|Vendor whereNameEn($value)
 * @method static Builder|Vendor whereNameRu($value)
 * @method static Builder|Vendor whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Vendor extends LocalizableModel
{
    /**
     * @var array
     */
    protected $localizable = ['name'];

    /**
     * @return BelongsToMany|InventoryItem[]|Collection
     */
    public function inventoryItems()
    {
        return $this->belongsToMany('App\Models\InventoryItem', 'vendor_inventory_item')->withPivot(['quantity']);
    }
}
