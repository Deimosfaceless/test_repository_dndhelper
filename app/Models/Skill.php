<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\Skill
 *
 * @property int $id
 * @property string $name_ru
 * @property string|null $name_en
 * @property int|null $parent
 * @property string|null $category_id
 * @property int|null $one_of
 * @property int|null $level
 * @property int|null $for_hero
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property string|null $description_admin
 * @property int|null $author_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read SkillCategory|null $category
 * @method static Builder|Skill newModelQuery()
 * @method static Builder|Skill newQuery()
 * @method static Builder|Skill query()
 * @method static Builder|Skill whereAuthorId($value)
 * @method static Builder|Skill whereCategoryId($value)
 * @method static Builder|Skill whereCreatedAt($value)
 * @method static Builder|Skill whereDescriptionAdmin($value)
 * @method static Builder|Skill whereDescriptionEn($value)
 * @method static Builder|Skill whereDescriptionRu($value)
 * @method static Builder|Skill whereForHero($value)
 * @method static Builder|Skill whereId($value)
 * @method static Builder|Skill whereLevel($value)
 * @method static Builder|Skill whereNameEn($value)
 * @method static Builder|Skill whereNameRu($value)
 * @method static Builder|Skill whereOneOf($value)
 * @method static Builder|Skill whereParent($value)
 * @method static Builder|Skill whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Skill extends LocalizableModel
{
    protected $localizable = ['name'];

    /**
     * @return BelongsTo|SkillCategory[]|Collection
     */
    public function category()
    {
        return $this->belongsTo('App\Models\SkillCategory');
    }
}
