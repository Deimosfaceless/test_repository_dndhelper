<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Hero
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name
 * @property float $gold
 * @property int|null $master_id
 * @property int|null $player_id
 * @property string|null $name_en
 * @property float|null $bio
 * @property string|null $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $world_id
 *
 * @property-read Collection|InventoryItem[] $inventoryItems
 * @property-read int|null $inventory_items_count
 * @property-read Collection|Skill[] $skills
 * @property-read Collection|Skill[] $availableSkills
 * @property-read World $world
 * @property-read int|null $skills_count
 * @property-read int|null $available_skills_count
 *
 * @method static Builder|Hero newModelQuery()
 * @method static Builder|Hero newQuery()
 * @method static Builder|Hero query()
 * @method static Builder|Hero whereBio($value)
 * @method static Builder|Hero whereCreatedAt($value)
 * @method static Builder|Hero whereGold($value)
 * @method static Builder|Hero whereId($value)
 * @method static Builder|Hero whereMasterId($value)
 * @method static Builder|Hero whereNameEn($value)
 * @method static Builder|Hero whereNameRu($value)
 * @method static Builder|Hero wherePlayerId($value)
 * @method static Builder|Hero whereSlug($value)
 * @method static Builder|Hero whereUpdatedAt($value)
 * @method static Builder|Hero whereWorldId($value)
 *
 * @mixin Eloquent
 */
class Hero extends LocalizableModel
{
    protected $localizable = ['name'];

    /**
     * @return BelongsToMany|Collection|InventoryItem[]
     */
    public function inventoryItems()
    {
        return $this->belongsToMany('App\Models\InventoryItem')->withPivot('quantity');
    }

    /**
     * @return BelongsToMany|Collection|Skill[]
     */
    public function skills()
    {
        return $this->belongsToMany('App\Models\Skill')->with('category')
            ->withPivot('on_level')->orderBy('on_level')
            ;
    }

    public function availableSkills()
    {
        return $this->belongsToMany('App\Models\Skill', 'hero_available_skill');
    }

    /**
     * @param $id
     *
     * @return Hero|Hero[]|Collection|Model
     */
    public static function getHeroById($id)
    {
        $hero = Hero::findOrFail($id);

        return $hero;
    }

    /**
     * @return BelongsTo|World
     */
    public function world()
    {
        return $this->belongsTo(World::class);
    }

    /**
     * @return HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
