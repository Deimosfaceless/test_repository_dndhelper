<?php

namespace App\Service;

use App\Contracts\UserManagerContract;
use App\Dto\UserData;
use App\User;

class UserManager implements UserManagerContract
{
    public function signUp(UserData $data): User
    {
        $user = new User(
            [
                'name' => $data->name,
                'email' => $data->email,
                'password' => bcrypt($data->password),
                'role' => $data->role ?? User::PLAYER_ROLE,
            ]);

        $user->save();

        return $user;
    }
}
