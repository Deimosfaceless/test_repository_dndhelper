<?php

namespace App\Transformers;

use App\Models\Hero;
use Flugg\Responder\Transformers\Transformer;

class HeroTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  Hero $hero
     * @return array
     */
    public function transform(Hero $hero)
    {
        return [
            'id' => (int) $hero->id,
            'name' => (string) $hero->name,
        ];
    }
}
