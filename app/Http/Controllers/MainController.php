<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;

class MainController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $files = File::allFiles(public_path() . '/img/main_img');
        $file = $files[rand(0, count($files) - 1)];

        $data['filename'] = $file->getFilename();

        return view('index')->with($data);
    }
}
