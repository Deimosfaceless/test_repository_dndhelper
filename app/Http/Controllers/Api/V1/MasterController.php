<?php

namespace App\Http\Controllers\Api\V1;

class MasterController extends ApiController
{
    public function getHeroesList()
    {
        $heroes = \Auth::user()->masteredHeroes;

        $this->setData($heroes);
        return $this->success();
    }
}
