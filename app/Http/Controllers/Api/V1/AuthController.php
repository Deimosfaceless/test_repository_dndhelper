<?php

namespace App\Http\Controllers\Api\V1;

use App\Dto\UserData;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\SignupRequest;
use App\Service\UserManager;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends ApiController
{
    /**
     * Регистрация пользователей через имейл и пароль
     *
     * @param SignupRequest $request
     *
     * @return JsonResponse
     */
    public function signup(SignupRequest $request)
    {
        $userData = new UserData([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'role' => $request->role ?? User::PLAYER_ROLE,
        ]);

        $userManager = app(UserManager::class);
        $user = $userManager->signUp($userData);

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();

        $this->setData([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
        ]);

        $this->setStatusCode(Response::HTTP_CREATED);
        return $this->success();
    }

    /**
     * @param LoginRequest $request
     *
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            $this->setStatusCode(Response::HTTP_UNAUTHORIZED);
            return $this->error('Unauthorized','Unauthorized');
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        $this->setData([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
        ]);

        return $this->success();
    }

    public function signupBySocial(Request $request)
    {
        $request->validate(
            [
                'client' => 'required|string',
                'token' => 'required|string',
            ]);
        dd(Socialite::driver('google')->userFromToken($request->token));
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        //'message' => 'Successfully logged out',
        return $this->respond();
    }
}
