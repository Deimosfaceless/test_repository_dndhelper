<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * @var int $statusCode
     */
    protected $statusCode = 200;

    protected $headers = [];

    protected $data = null;

    protected $transformer = null;

    /**
     * Get the status code.
     *
     * @return int $statusCode
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Set the status code.
     *
     * @param $statusCode
     *
     * @return $this
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param mixed|null $data
     */
    public function setData($data = null)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function success()
    {
        return responder()->success($this->data, $this->transformer)->respond($this->statusCode, $this->headers);
    }

    /**
     * @param string|null $errorCode
     * @param string|null $message
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function error(string $errorCode = null, string $message = null)
    {
        return responder()->error($errorCode, $message)->respond($this->statusCode, $this->headers);
    }
}
