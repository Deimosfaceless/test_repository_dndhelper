<?php

namespace App\Http\Controllers\Hero;

use App\Http\Controllers\Controller;
use App\Models\Hero;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;

class MasterHeroController extends Controller
{
    public function heroes()
    {
        $master = \Auth::user();

        $data = [
            'heroes' => $master->masteredHeroes
        ];

        return view('heroes.master.index', $data);
    }
}
