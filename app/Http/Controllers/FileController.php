<?php
namespace App\Http\Controllers;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class FileController extends Controller
{
    /** TODO DELETE ALL THIS CODE */
    private $image;
    public function __construct(File $image)
    {
        $this->image = $image;
    }
    public function getImages()
    {
        return view('images')->with('images', auth()->user()->images);
    }
    public function postUpload(Request $request)
    {
        $path = Storage::disk('s3')->put('images/originals', $request->file, 'public');
        $request->merge([
            'size' => $request->file->getClientSize(),
            'path' => $path
        ]);
        $this->image->create($request->only('path', 'title', 'size'));
        return back()->with('success', 'Image Successfully Saved');
    }
}
