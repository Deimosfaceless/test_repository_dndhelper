<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Laravel\Facades\Telegram;

class SettingController extends Controller
{
    public function index()
    {
        $data = Setting::getSettings();

        return view('admin.setting', $data);
    }

    public function store(Request $request)
    {
        Setting::whereNull('key')->orWhereNull('value')->delete();

        foreach ($request->except('_token') as $key => $value){
            $setting = Setting::where('key', '=', $key)->first() ? Setting::where('key', '=', $key)->first() : new Setting;
            $setting->key = $key;
            $setting->value = $request->$key;
            $setting->save();
        }

        return redirect()->route('admin.setting.index');
    }

    public function setWebhook(Request $request)
    {
        $result = $this->sendTelegramData('setwebhook', [
            'query' => ['url' => $request->url . '/' . \Telegram::getAccessToken()]
        ]);

        return redirect()->route('admin.setting.index')->with('status', $result);
    }

    public function getWebhookInfo(Request $request)
    {
        $result = $this->sendTelegramData('getwebhookinfo');

        return redirect()->route('admin.setting.index')->with('status', $result);
    }

    public function sendTelegramData($route = '', $params = [], $method = 'POST')
    {
        $client = new Client(['verify' => false, 'base_uri' => 'https://api.telegram.org/bot' . Telegram::getAccessToken() . '/']);
        $result = $client->request($method, $route, $params);

        return (string) $result->getBody();
    }
}
