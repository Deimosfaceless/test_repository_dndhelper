<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Http\Requests\Room\StoreRoomRequest;
use App\Models\Hero;
use App\Models\Message;
use App\Models\Room;
use App\Models\RoomMember;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class RoomController extends Controller
{
    public function index()
    {
        return view('chat.all_rooms');
    }

    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'message' => $request->input('message'),
            'room_id' => $request->get('room_id'),
        ]);

        broadcast(new MessageSent($user, $message))->toOthers();

        return ['status' => 'Message Sent!'];
    }

    public function fetchMessages()
    {
        return Message::with('sender')->get();
    }

    /**
     * Delete
     */

    //Все комнаты
    public function allRooms()
    {
        $this->redirectUnauthenticatedRoomMember();

        return view('chat.all_rooms', ['rooms' => Room::all()]);
    }

    /**
     * @return Redirector|null
     */
    private function redirectUnauthenticatedRoomMember(): ?Redirector
    {
        if(!Auth::user()){
            $roomMember = RoomMember::getGuestCurrentRoom();
            if($roomMember){
                return redirect(route('rooms.room', $roomMember->room_id));
            }
        }

        return null;
    }

    //Конкретная комната
    public function room(Request $request)
    {
        return view('chat.room')->with([
            'room' => Room::findOrFail($request->id),
            //'messages' => Message::where('room_id', $request->id),
        ]);
    }

    //Создание комнаты
    public function create()
    {
        return view('chat.create_room');
    }

    public function store(StoreRoomRequest $request)
    {
        $room = new Room();
        $room->creator_id = Auth::user()->id;
        $room->title = $request->get('title');
        $room->short_description = $request->get('short_description');
        $room->creator_type = User::class;

        $room->save();

        return redirect(route('rooms.create'));
    }

    public function join(Request $request)
    {
        RoomMember::firstOrCreate([
            'room_id' => $request->id,
            'member_id' => $request->get('member_id') ?? Auth::user()->id ?? $request->session()->getId(),
            'member_type' => $request->get('member_type') ?? Auth::user() ? User::class : 'guest'
        ]);

        return redirect( route('rooms.room', $request->id));
    }

    //Комнаты созданные пользователем
    public function userRooms()
    {

    }
}
