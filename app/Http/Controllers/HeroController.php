<?php

namespace App\Http\Controllers;

use App\Models\Hero;
use Illuminate\Http\Request;

class HeroController extends Controller
{
    public function index()
    {
        $data = [];
        return view('heroes.index', $data);
    }

    public function show($id)
    {
        $hero = Hero::getHeroById($id);
        $data['hero'] = $hero;

        $skills = $hero->skills;

        $data['skills'] = $skills;
//        dd($skills[1]->category->shade);
        $data['skill_cats'] = $skills->groupBy('category_id')->keys()->flip();

        return view('heroes.show', $data);
    }
}
