<?php

namespace App\Contracts;

use App\Dto\UserData;

interface UserManagerContract
{
    public function signUp(UserData $userData);
}
